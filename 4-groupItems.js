//Group items based on the Vitamins that they contain in the following format:
// {
//     "Vitamin C": ["Orange", "Mango"],
//     "Vitamin K": ["Mango"],
// }
let items = require("./3-arrays-vitamins.cjs");

let groupItems = (items) => {
  return items.reduce((acc, obj) => {
    obj.contains.split(",").forEach((val) => {
      if (acc[val.trim()] !== undefined) {
        acc[val.trim()].push(obj.name);
      } else {
        acc[val.trim()] = [obj.name];
      }
    });
    return acc;
  }, {});
};

console.log(groupItems(items));
