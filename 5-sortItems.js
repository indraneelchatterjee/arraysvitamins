// 5. Sort items based on number of Vitamins they contain.
let items = require("./3-arrays-vitamins.cjs");

let sorting = (items) => {
  return items.sort(
    (a, b) => a["contains"].split(",").length - b["contains"].split(",").length
  );
};
console.log(sorting(items));
